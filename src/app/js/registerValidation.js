var togglePassword = function() {

    //setting checkbox state to the oppsite of the current one (this is necessary if the user clicks on the div around "Anzeigen")
    $('#show-password').prop('checked', !$('#show-password').is(':checked'));

    //Change field type according to 'show password' checkbox
    if ($('#show-password').is(':checked')) {
        $('#password').attr('type', 'text');
    } else {
        $('#password').attr('type', 'password');

    }

};


var passwordSecurity = function() {
    var password = $('#password').val();
    var indicator = '#strength-indicator';
    var baseClass = 'password-strength';
    addRemoveElementClass(indicator, '', baseClass);
    $("#strength-name").html('');

    if (password) {

        /*check if length 5 characters or more*/
        if (password.length >= 4) {
            addRemoveElementClass(indicator, 'very-weak', baseClass, 'Very Weak');
        }

        if (password.length >= 6) {
            /* check if contains lowercase characters or numbers*/
            if (password.match(/[a-z]+/) || password.match(/[0-9]+/)) {
                addRemoveElementClass(indicator, 'weak', baseClass, 'Weak');
            }

            /*check for numbers and lowercase characters*/
            if (password.match(/[0-9]+/) && password.match(/[a-z]+/)) {
                addRemoveElementClass(indicator, 'good', baseClass, 'Good');
            }

            /*check for numbers, lowercase and uppercase characters*/
            if (password.match(/[A-Z]+/) && password.match(/[0-9]+/) && password.match(/[a-z]+/)) {
                addRemoveElementClass(indicator, 'strong', baseClass, 'Strong');
            }

            /*check for numbers, lowercase, uppercase, and 20 characters or more*/
            if (password.match(/[A-Z]+/) && password.match(/[0-9]+/) && password.match(/[a-z]+/) && password.length >= 20) {
                addRemoveElementClass(indicator, 'very-strong', baseClass, 'Very Strong');
            }
        }

    }
};

var addRemoveElementClass = function(elementId, classToAdd, classTokeep, nameIndicator) {
    $(elementId).removeClass();
    $(elementId).addClass(classTokeep);
    $(elementId).addClass(classToAdd);

    //show password password taxt indicator
    if (nameIndicator) {
        $("#strength-name").html('Password Strength: ' + nameIndicator);
    }
}

$('#register-button').on('click', function(e) {
    if ($("#register-form")[0].checkValidity() && !$('#agree').prop('checked')) {
        alert('Please agree with the terms');
        e.preventDefault();
    }
});
